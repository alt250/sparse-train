/home/denis/app/anaconda3/envs/cifar10-fast/bin/python /home/denis/app/pycharm-community-2019.1.3/helpers/pydev/pydevd.py --multiproc --qt-support=auto --client 127.0.0.1 --port 39991 --file /home/denis/src/cifar10-fast/prunetrain_small_test.py --arch=vgg8 --prune_freq=100 --lasso_penalty_ratio=0.2
pydev debugger: process 2205 is connecting

Connected to pydev debugger (build 191.8026.44)
Namespace(arch='vgg8', data_dir='./data', lasso_penalty_ratio=0.2, log_dir='.', prune_freq=100, prune_threshold=0.0001)
Downloading datasets
Files already downloaded and verified
Files already downloaded and verified
Warming up cudnn on random inputs
stage1_block0_conv1	Conv2d	torch.Size([1, 64, 32, 32])
stage1_block0_bn1	BatchNorm2d	torch.Size([1, 64, 32, 32])
stage1_block0_relu1	ReLU	torch.Size([1, 64, 32, 32])
stage1_maxpool0	MaxPool2d	torch.Size([1, 64, 16, 16])
stage2_block0_conv1	Conv2d	torch.Size([1, 128, 16, 16])
stage2_block0_bn1	BatchNorm2d	torch.Size([1, 128, 16, 16])
stage2_block0_relu1	ReLU	torch.Size([1, 128, 16, 16])
stage2_maxpool0	MaxPool2d	torch.Size([1, 128, 8, 8])
stage3_block0_conv1	Conv2d	torch.Size([1, 256, 8, 8])
stage3_block0_bn1	BatchNorm2d	torch.Size([1, 256, 8, 8])
stage3_block0_relu1	ReLU	torch.Size([1, 256, 8, 8])
stage3_maxpool0	MaxPool2d	torch.Size([1, 256, 4, 4])
stage4_block0_conv1	Conv2d	torch.Size([1, 512, 4, 4])
stage4_block0_bn1	BatchNorm2d	torch.Size([1, 512, 4, 4])
stage4_block0_relu1	ReLU	torch.Size([1, 512, 4, 4])
stage4_maxpool0	MaxPool2d	torch.Size([1, 512, 2, 2])
stage5_block0_conv1	Conv2d	torch.Size([1, 512, 2, 2])
stage5_block0_bn1	BatchNorm2d	torch.Size([1, 512, 2, 2])
stage5_block0_relu1	ReLU	torch.Size([1, 512, 2, 2])
stage5_maxpool0	MaxPool2d	torch.Size([1, 512, 1, 1])
final_flatten	FlattenDim	torch.Size([1, 512])
final_fc	Linear	torch.Size([1, 10])
classifier	Identity	torch.Size([1, 10])
loss	<class 'torch.nn.modules.loss.CrossEntropyLoss'>	forward() missing 1 required positional argument: 'target'
correct	<class 'torch_backend.Correct'>	forward() missing 1 required positional argument: 'target'
Starting timer
Preprocessing training data
Finished in 1.1 seconds
Preprocessing test data
Finished in 0.04 seconds
set group_lasso_coeff=0.0003942116019652062 group_lasso_penalty_ratio=0.2 loss.mean()=2.447265625 lasso_penalty=1552.0
       epoch           lr   train time   train loss    train acc    test time    test loss     test acc   total time  train lasso   loss+lasso   prune time     channels     sparsity
           1       2.5600      10.3017       2.3714       0.1182       0.4992       2.3236       0.1377      10.8009       0.6118       2.9824       0.0000         4419       0.0000
           2       5.1200       9.6601       2.2510       0.1654       0.4867       2.1581       0.2093      20.9476       0.6115       2.8619       0.0000         4419       0.0000
           3       7.6800       9.8428       2.0813       0.2511       0.5244       1.9620       0.3191      31.3147       0.6111       2.6924       0.0000         4419       0.0000
           4      10.2400      10.0797       1.9076       0.3238       0.5216       1.7798       0.3859      41.9161       0.6102       2.5173       0.0000         4419       0.0000
           5      12.8000       9.6955       1.7564       0.3756       0.4949       1.6187       0.4400      52.1065       0.6082       2.3645       0.0000         4419       0.0000
           6      15.3600       9.7114       1.6223       0.4283       0.5318       1.4916       0.4865      62.3498       0.6009       2.2231       0.0000         4419       0.0000
           7      17.9200       9.8141       1.5209       0.4680       0.5287       1.4214       0.5094      72.6926       0.5812       2.1018       0.0000         4419       0.0000
           8      20.4800       9.7277       1.4483       0.4949       0.4908       1.3339       0.5487      82.9110       0.5492       1.9972       0.0000         4419       0.0000
           9      23.0400       9.8310       1.3938       0.5185       0.4917       1.2839       0.5681      93.2337       0.5062       1.8997       0.0000         4419       0.0000
          10      25.6000       9.6036       1.3543       0.5371       0.4928       1.2652       0.5822     103.3302       0.4577       1.8120       0.0000         4419       0.0000
          11      28.1600       9.6842       1.3344       0.5500       0.4913       1.2531       0.5958     113.5057       0.4125       1.7469       0.0000         4419       0.0000
          12      30.7200       9.6228       1.3249       0.5610       0.5123       1.2581       0.5943     123.6409       0.3706       1.6957       0.0000         4419       0.0000
          13      33.2800       9.6241       1.3207       0.5708       0.4959       1.2488       0.6114     133.7608       0.3293       1.6498       0.0000         4419       0.0000
          14      35.8400       9.6275       1.3268       0.5815       0.4913       1.2716       0.6224     143.8797       0.2886       1.6155       0.0000         4419       0.0000
          15      38.4000       9.8283       1.3433       0.5882       0.4947       1.2970       0.6263     154.2027       0.2486       1.5919       0.0000         4419       0.0000
          16      40.9600       9.7909       1.3679       0.5952       0.4973       1.3317       0.6196     164.4909       0.2128       1.5805       0.0000         4419       0.0000
          17      43.5200       9.8876       1.4050       0.6016       0.4963       1.3467       0.6225     174.8748       0.1853       1.5903       0.0000         4419       0.0000
          18      46.0800       9.6443       1.4515       0.6070       0.4991       1.4415       0.6166     185.0182       0.1615       1.6129       0.0000         4419       0.0000
          19      48.6400       9.5873       1.5106       0.6098       0.5017       1.5025       0.6142     195.1072       0.1393       1.6501       0.0000         4419       0.0000
          20      51.2000       9.8919       1.5769       0.6099       0.5258       1.5674       0.6330     205.5249       0.1184       1.6953       0.0000         4419       0.0000
          21      49.2544       9.8751       1.6463       0.6099       0.4994       1.6293       0.6295     215.8994       0.0988       1.7455       0.0000         4419       0.0000
          22      47.3088      10.0530       1.7096       0.6030       0.5093       1.7036       0.5970     226.4617       0.0832       1.7927       0.0000         4419       0.0000
          23      45.3632       9.9444       1.7600       0.5920       0.4910       1.7093       0.6063     236.8971       0.0726       1.8326       0.0000         4419       0.0000
          24      43.4176       9.8207       1.7932       0.5807       0.5010       1.7613       0.5931     247.2188       0.0635       1.8567       0.0000         4419       0.0000
          25      41.4720       9.8958       1.8170       0.5715       0.5189       1.7809       0.5521     257.6335       0.0558       1.8727       0.0000         4419       0.0000
          26      39.5264       9.6622       1.8351       0.5617       0.5138       1.7823       0.5622     267.8095       0.0505       1.8857       0.0000         4419       0.0000
          27      37.5808       9.7714       1.8474       0.5525       0.5318       1.7915       0.5579     278.1127       0.0463       1.8936       0.0000         4419       0.0000
          28      35.6352       9.8738       1.8587       0.5459       0.4943       1.8140       0.5536     288.4808       0.0424       1.9012       0.0000         4419       0.0000
          29      33.6896       9.9746       1.8671       0.5376       0.4936       1.8468       0.4876     298.9489       0.0392       1.9062       0.0000         4419       0.0000
          30      31.7440       9.5715       1.8748       0.5319       0.4913       1.8390       0.5402     309.0117       0.0368       1.9115       0.0000         4419       0.0000
          31      29.7984       9.6595       1.8789       0.5261       0.4923       1.8191       0.5203     319.1635       0.0350       1.9141       0.0000         4419       0.0000
          32      27.8528       9.7635       1.8857       0.5201       0.5060       1.8373       0.5267     329.4330       0.0330       1.9186       0.0000         4419       0.0000
          33      25.9072       9.5951       1.8902       0.5133       0.5050       1.8443       0.4944     339.5330       0.0319       1.9221       0.0000         4419       0.0000
penalty=nan
penalty=nan
penalty=nan
penalty=nan

