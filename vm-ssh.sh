#!/bin/bash

gcloud compute ssh --ssh-flag="-p 443" --zone="europe-west4-c" jupyter@pytorch-colab-backend -- -L 8080:localhost:8080

