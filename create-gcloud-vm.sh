#!/bin/bash

# Create GCP VM with P4 GPU
# sources: 
#   https://course.fast.ai/start_gcp.html
#   https://github.com/davidcpage/cifar10-fast/blob/master/batch_norm_post.ipynb

export IMAGE_FAMILY="pytorch-latest-gpu" # or "pytorch-latest-cpu" for non-GPU instances
export ZONE="europe-west4-c"
export INSTANCE_NAME="pytorch-colab-backend"
export INSTANCE_TYPE="n1-highmem-8" # budget: "n1-highmem-4"

# budget: 'type=nvidia-tesla-k80,count=1'
# medium: "type=nvidia-tesla-p4,count=1"
gcloud compute instances create $INSTANCE_NAME \
        --zone=$ZONE \
        --image-family=$IMAGE_FAMILY \
        --image-project=deeplearning-platform-release \
        --maintenance-policy=TERMINATE \
        --accelerator="type=nvidia-tesla-v100,count=1" \
        --machine-type=$INSTANCE_TYPE \
        --boot-disk-size=200GB \
        --metadata="install-nvidia-driver=True" \
        --preemptible

